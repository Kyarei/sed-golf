import itertools
import random
import re
from typing import List, Iterable, Tuple, Set, Dict
import Levenshtein as lev


def make_alphabet(*lists):
    alphabet_set = set()
    for l in lists:
        for s in l:
            alphabet_set |= set(s)
    return "".join(alphabet_set)


SPECIAL_CHARS = re.compile("[][\\?*+{}/]")


def escape_slashes(s: str) -> str:
    return s.replace("\\", "\\\\").replace("/", "\\/")


class Rule:
    def apply(self, s: str) -> str:
        raise NotImplementedError("Should be implemented by subclass")

    def as_sed_rule(self) -> str:
        raise NotImplementedError("Should be implemented by subclass")

    def tinker(self, alphabet_in: str, alphabet_out: str):
        raise NotImplementedError("Should be implemented by subclass")


BOUNDARY = '\\b'


class SRule(Rule):
    def __init__(self, left: str, right: str, mode: int):
        self.left = left
        self.right = right
        self.mode = mode
        self.re = re.compile(
            f"{BOUNDARY if self.mode == 1 else ''}{re.escape(self.left)}{BOUNDARY if self.mode == 2 else ''}")

    def apply(self, s: str) -> str:
        return re.sub(self.re, lambda _: self.right, s)

    def as_sed_rule(self) -> str:
        return f"s/{BOUNDARY if self.mode == 1 else ''}{re.escape(self.left)}{BOUNDARY if self.mode == 2 else ''}/{re.escape(self.right)}/g"

    def tinker(self, alphabet_in: str, alphabet_out: str) -> Iterable[Rule]:
        # Tinker with the left hand side
        for i in range(len(self.left)):
            # yield SRule(self.left[0:i] + self.left[i + 1:], self.right, self.mode)
            for letter in alphabet_in:
                # yield SRule(self.left[0:i] + letter + self.left[i:], self.right, self.mode)
                yield SRule(self.left[0:i] + letter + self.left[i + 1:], self.right, self.mode)
        # Tinker with the right hand side
        for i in range(len(self.right)):
            # yield SRule(self.left, self.right[0:i] + self.right[i + 1:], self.mode)
            for letter in alphabet_out:
                # yield SRule(self.left, self.right[0:i] + letter + self.right[i:], self.mode)
                yield SRule(self.left, self.right[0:i] + letter + self.right[i + 1:], self.mode)


class YRule(Rule):
    def __init__(self, left: str, right: str):
        assert len(left) == len(right)
        self.left = left
        self.right = right
        self.table = str.maketrans(left, right)

    def apply(self, s: str) -> str:
        return s.translate(self.table)

    def as_sed_rule(self) -> str:
        return f"y/{escape_slashes(self.left)}/{escape_slashes(self.right)}/"

    def tinker(self, alphabet_in: str, alphabet_out: str) -> Iterable[Rule]:
        # Tinker with the left hand side
        for i in range(len(self.left)):
            for letter in alphabet_in:
                if not (letter in self.left):
                    yield YRule(self.left[0:i] + letter + self.left[i + 1:], self.right)
        # Tinker with the right hand side
        for i in range(len(self.right)):
            for letter in alphabet_out:
                if not (letter in self.right):
                    yield YRule(self.left, self.right[0:i] + letter + self.right[i + 1:])


def score_words(result: List[str], goal: List[str]) -> int:
    return sum((d * d for d in (lev.distance(p[0], p[1]) for p in
                                itertools.zip_longest(result, goal))))


def make_y_rule(choose_letter_in, choose_letter_out) -> Rule:
    numchars = random.randint(3, 8)
    left = "".join(choose_letter_in() for _ in range(numchars))
    right = "".join(choose_letter_out() for _ in range(numchars))
    return YRule(left, right)


def make_s_rule(choose_letter_in, choose_letter_out) -> Rule:
    numchars_left = random.randint(1, 3)
    numchars_right = random.randint(1, 3)
    left = "".join(choose_letter_in() for _ in range(numchars_left))
    right = "".join(choose_letter_out() for _ in range(numchars_right))
    mode = random.choices([0, 1, 2], [0.6, 0.2, 0.2])[0]
    return SRule(left, right, mode)


def make_inserter(choose_letter_in, choose_letter_out) -> Rule:
    inserted = choose_letter_out()
    numchars_left = random.randint(0, 2)
    numchars_right = random.randint(0, 2)
    left = "".join(choose_letter_in() for _ in range(numchars_left))
    right = "".join(choose_letter_out() for _ in range(numchars_right))
    mode = random.choices([0, 1, 2], [0.8, 0.1, 0.1])[0]
    return SRule(left + right, left + inserted + right, mode)


def make_deleter(choose_letter_in, choose_letter_out) -> Rule:
    deleted = choose_letter_in()
    numchars_left = random.randint(0, 2)
    numchars_right = random.randint(0, 2)
    left = "".join(choose_letter_in() for _ in range(numchars_left))
    right = "".join(choose_letter_out() for _ in range(numchars_right))
    mode = random.choices([0, 1, 2], [0.8, 0.1, 0.1])[0]
    return SRule(left + deleted + right, left + right, mode)


def make_replacer(choose_letter_in, choose_letter_out) -> Rule:
    inserted = choose_letter_out()
    deleted = choose_letter_in()
    numchars_left = random.randint(0, 2)
    numchars_right = random.randint(0, 2)
    left = "".join(choose_letter_in() for _ in range(numchars_left))
    right = "".join(choose_letter_out() for _ in range(numchars_right))
    mode = random.choices([0, 1, 2], [0.8, 0.1, 0.1])[0]
    return SRule(left + deleted + right, left + inserted + right, mode)


RULES = [(make_s_rule, 5),
         (make_inserter, 2), (make_deleter, 2), (make_replacer, 2)]


def make_random_rule(alphabet_in: str, alphabet_out: str) -> Rule:
    def choose_letter(alphabet):
        alen = len(alphabet)
        return lambda: alphabet[random.randrange(0, alen)]

    rule_maker = random.choices([e[0] for e in RULES], [
                                e[1] for e in RULES])[0]
    return rule_maker(choose_letter(alphabet_in), choose_letter(alphabet_out))


def tinker_with_rule(rule: Rule, input: List[str], expected: List[str], alphabet_in: str, alphabet_out: str, candidates: int = 100) -> Tuple[Rule, List[str]]:
    current_iter = [rule.apply(s) for s in input]
    current_rule = rule
    if candidates < 10:
        return (current_rule, current_iter)
    current_score = score_words(current_iter, expected)
    while True:
        best = current_score
        best_results = None
        best_rule = None
        rules = list(current_rule.tinker(alphabet_in, alphabet_out))
        if len(rules) > candidates:
            rules = random.sample(rules, k=candidates)
        for rule in rules:
            results = [rule.apply(s) for s in input]
            if len(set(results)) < len(results):
                continue
            score = score_words(results, expected)
            if score < best:
                best = score
                best_results = results
                best_rule = rule
        if not best_results:
            break
        current_rule = best_rule
        current_iter = best_results
        current_score = best
    return (current_rule, current_iter)


def informed_ops(input: str, output: str, reach: int) -> Iterable[Tuple]:
    opcodes = lev.opcodes(input, output)
    for opcode in opcodes:
        inst, i1, i2, j1, j2 = opcode
        i0 = max(0, i1 - reach)
        i3 = min(len(input), i2 + reach)
        la = i1 <= reach
        ra = i1 >= len(input) - reach
        if inst == 'replace':
            yield ('replace', input[i1:i2], output[j1:j2], input[i0:i1], input[i2:i3], la, ra)
        elif inst == 'insert':
            yield ('insert', output[j1:j2], input[i0:i1], input[i2:i3], la, ra)
        elif inst == 'delete':
            yield ('delete', input[i1:i2], input[i0:i1], input[i2:i3], la, ra)


def informed_ops_all(inputs: List[str], outputs: List[str], reach: int) -> Set[Tuple]:
    res = set()
    for input, output in itertools.zip_longest(inputs, outputs):
        for opcode in informed_ops(input, output, reach):
            res.add(opcode)
    return res


def subops(opcode: Tuple, reach: int) -> Iterable[Tuple]:
    inst = opcode[0]
    if inst == 'replace':
        inst, s1, s2, b, a, la, ra = opcode
        l1 = len(s1)
        l2 = len(s2)
        for i1 in range(0, l1):
            for i2 in range(i1 + 1, l1 + 1):
                bb = b + s1[:i1]
                bb = bb[max(0, len(bb) - reach):]
                aa = s1[i2:] + a
                aa = aa[:min(len(aa), reach)]
                m = s1[i1:i2]
                for j1 in range(0, l2):
                    for j2 in range(j1 + 1, l2 + 1):
                        n = s2[j1:j2]
                        for j in range(0, len(bb) + 1):
                            for k in range(0, len(aa) + 1):
                                yield ('replace', m, n, bb[j:], aa[:k], False, False)
                        if la:
                            for k in range(0, len(aa) + 1):
                                yield ('replace', m, n, bb, aa[:k], True, False)
                        if ra:
                            for j in range(0, len(bb) + 1):
                                yield ('replace', m, n, bb[j:], aa, False, True)
    elif inst == 'insert':
        inst, s, b, a, la, ra = opcode
        l = len(s)
        for i1 in range(0, l):
            for i2 in range(i1 + 1, l + 1):
                m = s[i1:i2]
                for j in range(0, len(b) + 1):
                    for k in range(0, len(a) + 1):
                        yield ('insert', m, b[j:], a[:k], False, False)
                if la:
                    for k in range(0, len(a) + 1):
                        yield ('insert', m, b, a[:k], True, False)
                if ra:
                    for j in range(0, len(b) + 1):
                        yield ('insert', m, b[j:], a, False, True)
    elif inst == 'delete':
        inst, s, b, a, la, ra = opcode
        l = len(s)
        for i1 in range(0, l):
            for i2 in range(i1 + 1, l + 1):
                bb = b + s[:i1]
                bb = bb[max(0, len(bb) - reach):]
                aa = s[i2:] + a
                aa = aa[:min(len(aa), reach)]
                m = s[i1:i2]
                for j in range(0, len(bb) + 1):
                    for k in range(0, len(aa) + 1):
                        yield ('delete', m, bb[j:], aa[:k], False, False)
                if la:
                    for k in range(0, len(aa) + 1):
                        yield ('delete', s[i1:i2], bb, aa[:k], True, False)
                if ra:
                    for j in range(0, len(bb) + 1):
                        yield ('delete', s[i1:i2], bb[j:], aa, False, True)


def informed_ops_and_subops(inputs: List[str], outputs: List[str], reach: int) -> Set[Tuple]:
    res = set()
    for opcode in informed_ops_all(inputs, outputs, reach):
        for o in subops(opcode, reach):
            res.add(o)
    return res


def op_as_rule(opcode: Tuple) -> Rule:
    inst = opcode[0]
    if inst == 'replace':
        return SRule(opcode[3] + opcode[1] + opcode[4], opcode[3] + opcode[2] + opcode[4], opcode[5] + 2 * opcode[6])
    elif inst == 'insert':
        return SRule(opcode[2] + opcode[3], opcode[2] + opcode[1] + opcode[3], opcode[4] + 2 * opcode[5])
    elif inst == 'delete':
        return SRule(opcode[2] + opcode[1] + opcode[3], opcode[2] + opcode[3], opcode[4] + 2 * opcode[5])


def get_candidate_rules(inputs: List[str], outputs: List[str], reach: int) -> List[Rule]:
    return list(filter(lambda r: r.left, map(op_as_rule, informed_ops_and_subops(inputs, outputs, reach))))


def get_best_transposition(inputs: List[str], outputs: List[str], alphabet_in: str, alphabet_out: str) -> Dict[str, str]:
    trans = {}
    current_iter = inputs
    current_score = score_words(inputs, outputs)
    alphabet_in = random.sample(alphabet_in, k=len(alphabet_in))
    alphabet_out = random.sample(alphabet_out, k=len(alphabet_out))
    while True:
        best_score = current_score
        best_results = None
        best_trans_key = None
        best_trans_value = None
        for k in alphabet_in:
            for v in alphabet_out:
                results = [s.translate({ord(k): ord(v)}) for s in current_iter]
                if len(set(results)) < len(results):
                    continue
                score = score_words(results, outputs)
                if score < best_score:
                    best_results = results
                    best_trans_key = k
                    best_trans_value = v
                    best_score = score
        if not best_results:
            break
        trans[best_trans_key] = best_trans_value
        current_iter = best_results
        current_score = best_score
    return trans


def get_best_transposition_as_rule(inputs: List[str], outputs: List[str], alphabet_in: str, alphabet_out: str) -> YRule:
    res = get_best_transposition(inputs, outputs, alphabet_in, alphabet_out)
    a, b = "", ""
    for k, v in res.items():
        a += k
        b += v
    return YRule(a, b)
