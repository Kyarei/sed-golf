#!/usr/bin/env python3

"""
A program to play sed golf.
"""

import itertools
import random
import re
from typing import List, Iterable, Tuple
import Levenshtein as lev

from rulelib import *

# MAIN

with open("entries.txt") as fh:
    entries = [line.split() for line in fh.readlines()]

print(entries)

keys: List[str] = [e[0] for e in entries]
values: List[str] = [e[1] for e in entries]

alphabet = make_alphabet(keys, values)
alphabet_out = make_alphabet(values)

print(alphabet)

assert len(set(keys)) == len(entries)


current_iter = keys
total_iters = 0
MAX_ITERS = 50000
BRANCH_WIDTH = 100
current_score = score_words(keys, values)
our_rules = []
consecutive_fails = 0

print(f"Score starts at {current_score}")

try:
    print("Inferring translation table...")
    trans_rule = get_best_transposition_as_rule(
        current_iter, values, make_alphabet(current_iter), alphabet_out)
    if trans_rule.left:
        our_rules.append(trans_rule)
        current_iter = [trans_rule.apply(s) for s in current_iter]
        current_score = score_words(current_iter, values)

    while True:
        total_iters += 1
        if consecutive_fails > 0:
            print(
                f"Before iteration {total_iters}, score is {current_score} ({consecutive_fails} consecutive {'failures' if consecutive_fails > 1 else 'failure'})")
        else:
            print(
                f"Before iteration {total_iters}, score is {current_score}")
        print(f"{len(our_rules)} {'rule' if len(our_rules) == 1 else 'rules'} so far.")
        if total_iters >= MAX_ITERS or current_score == 0:
            break
        alphabet_in = make_alphabet(current_iter)
        # Try systematic rules
        rules = get_candidate_rules(
            current_iter, values, 2 + consecutive_fails)
        print(len(rules))
        best = current_score
        best_results = None
        best_rule = None
        for rule in rules:
            results = [rule.apply(s) for s in current_iter]
            if len(set(results)) < len(results):
                continue
            score = score_words(results, values)
            if score < best:
                best = score
                best_results = results
                best_rule = rule
        # Try random rules -- these need tinkering
        tinkers = 100 + 25 * consecutive_fails
        rules = [make_random_rule(alphabet_in, alphabet_out)
                 for _ in range(BRANCH_WIDTH)]
        for rule in rules:
            rule2, results = tinker_with_rule(
                rule, current_iter, values, alphabet_in, alphabet_out, tinkers)
            if len(set(results)) < len(results):
                continue
            score = score_words(results, values)
            if score < best:
                best = score
                best_results = results
                best_rule = rule2
        if best_results == None:
            consecutive_fails += 1
            continue
        consecutive_fails = 0
        current_score = best
        current_iter = best_results
        our_rules.append(best_rule)
except KeyboardInterrupt:
    print("*** INTERRUPTED ***")

print("\nFinal results:\n")
for k, v in itertools.zip_longest(keys, current_iter):
    print(f"{k} {v}")

print(f"\nFinished run with score {current_score}:\n\n")

for rule in our_rules:
    print(rule.as_sed_rule())

print("\n\n")
you_are_already_dead = "omae ha mou shindeiru."
for rule in our_rules:
    you_are_already_dead = rule.apply(you_are_already_dead)
print(you_are_already_dead)
