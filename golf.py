#!/usr/bin/env python3

"""
A program to play sed golf.
"""

import itertools
import random
import re
from typing import List
import Levenshtein as lev

from rulelib import *

# MAIN


with open("entries.txt") as fh:
    entries = [line.split() for line in fh.readlines()]

print(entries)

keys: List[str] = [e[0] for e in entries]
values: List[str] = [e[1] for e in entries]

alphabet = make_alphabet(keys, values)
alphabet_out = make_alphabet(values)

print(alphabet)

assert len(set(keys)) == len(entries)


current_iter = keys
total_iters = 0
MAX_ITERS = 500000
BRANCH_WIDTH = 100
current_score = score_words(keys, values)
our_rules = []

print(f"Score starts at {current_score}")

while True:
    total_iters += 1
    if total_iters % 100 == 0:
        print(f"Before iteration {total_iters}, score is {current_score}")
    if total_iters >= MAX_ITERS:
        break
    alphabet_in = make_alphabet(current_iter)
    rules = [make_random_rule(alphabet_in, alphabet_out)
             for _ in range(BRANCH_WIDTH)]
    best = current_score
    best_results = None
    best_rule = None
    for rule in rules:
        results = list(map(lambda s: rule.apply(s), current_iter))
        if len(set(results)) < len(results):
            continue
        score = score_words(results, values)
        if score < best:
            best = score
            best_results = results
            best_rule = rule
    if best_results == None:
        continue
    current_score = best
    current_iter = best_results
    our_rules.append(best_rule)

print("\nFinal results:\n")
for k, v in itertools.zip_longest(keys, current_iter):
    print(f"{k} {v}")

print(f"\nFinished run with score {current_score}:\n\n")

for rule in our_rules:
    print(rule.as_sed_rule())

print("\n\n")
you_are_already_dead = "omae ha mou shindeiru."
for rule in our_rules:
    you_are_already_dead = rule.apply(you_are_already_dead)
print(you_are_already_dead)
